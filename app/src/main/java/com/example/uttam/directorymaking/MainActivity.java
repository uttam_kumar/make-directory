package com.example.uttam.directorymaking;

import java.io.File;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onStart() {
        super.onStart();
        createFolderExternalStorage("doc_down_external");
        createFolderInternalStorage("doc_down_internal");
    }

    public void createFolderExternalStorage(String fname) {
        String myfolder = Environment.getExternalStorageDirectory() + "/" + fname;
        File f = new File(myfolder);
        if (!f.exists()) {

            if (!f.mkdir()) {
                Log.d("TAG", "createFolderExternalStorage: "+ "can't be created");
                //Toast.makeText(this, myfolder + " can't be created.", Toast.LENGTH_SHORT).show();

            } else {
                Log.d("TAG", "createFolderExternalStorage: "+ "can be created");
                //Toast.makeText(this, myfolder + " can be created.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.d("TAG", "createFolderExternalStorage: "+ "already exits");
            //Toast.makeText(this, myfolder + " already exits.", Toast.LENGTH_SHORT).show();
        }
    }



    public void createFolderInternalStorage(String fname) {

        File folder = getFilesDir();
        File f= new File(folder, fname);


        if (!f.exists()) {

            if (!f.mkdir()) {
                Log.d("TAG", "createFolderInternalStorage: "+ "can't be created");
                //Toast.makeText(this, folder + " can't be created.", Toast.LENGTH_SHORT).show();

            } else {
                Log.d("TAG", "createFolderInternalStorage: "+ "can be created");
                //Toast.makeText(this, folder + " can be created.", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.d("TAG", "createFolderInternalStorage: "+ "already exits");
            //Toast.makeText(this, folder + " already exits.", Toast.LENGTH_SHORT).show();
        }
    }

}